<?php

namespace Drupal\webform_portuguese_nif\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'webform_portuguese_nif'.
 *
 * Webform elements are just wrappers around form elements, therefore every
 * webform element must have correspond FormElement.
 *
 * Below is the definition for a custom 'webform_portuguese_nif' which just
 * renders a simple text field.
 *
 * @FormElement("webform_portuguese_nif")
 */
class WebformPortugueseNif extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#size' => 60,
      '#process' => [
        [$class, 'processWebformPortugueseNif'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateWebformPortugueseNif'],
      ],
      '#pre_render' => [
        [$class, 'preRenderWebformPortugueseNif'],
      ],
      '#theme' => 'input__webform_portuguese_nif',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Processes a 'webform_portuguese_nif' element.
   */
  public static function processWebformPortugueseNif(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here you can add and manipulate your element's properties and callbacks.
    return $element;
  }

  /**
   * Webform element validation handler for #type 'webform_portuguese_nif'.
   */
  public static function validateWebformPortugueseNif(&$element, FormStateInterface $form_state, &$complete_form) {
    $values = $form_state->getValues();
    if (!empty($values[$element['#name']])) {
      $nif = $values[$element['#name']];
      $valid = static::validateNif($nif);
      if (!$valid) {
        $form_state->setError($element, t('Invalid NIF'));
      }
    }
  }

  /**
   * Helper function to validate NIF number.
   */
  public static function validateNif($nif, $ignore_first = TRUE) {
    if (!is_numeric($nif) || strlen($nif) != 9) {
      return FALSE;
    }
    else {
      $nif_split = str_split($nif);
      if (in_array($nif_split[0], [1, 2, 3, 5, 6, 7, 8, 9]) || $ignore_first) {
        $check_digit = 0;
        for ($i = 0; $i < 8; $i++) {
          $check_digit += $nif_split[$i] * (10 - $i - 1);
        }
        $check_digit = 11 - ($check_digit % 11);
        if ($check_digit >= 10) {
          $check_digit = 0;
        }
        if ($check_digit == $nif_split[8]) {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * Prepares a #type 'text' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderWebformPortugueseNif(array $element) {
    $element['#attributes']['type'] = 'text';
    $element_attributes = [
      'id',
      'name',
      'value',
      'size',
      'maxlength',
      'placeholder',
    ];
    Element::setAttributes($element, $element_attributes);
    static::setAttributes($element, ['form-text', 'webform-nif-element']);
    return $element;
  }

}

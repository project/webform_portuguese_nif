<?php

namespace Drupal\webform_portuguese_nif\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;

/**
 * Provides a 'webform_portuguese_nif' element.
 *
 * @WebformElement(
 *   id = "webform_portuguese_nif",
 *   label = @Translation("Portuguese NIF"),
 *   description = @Translation("Provides a webform element for nif."),
 *   category = @Translation("Custom"),
 * )
 */
class WebformPortugueseNif extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {

    return [
      'multiple' => '',
      'size' => '',
      'minlength' => '',
      'maxlength' => '',
      'placeholder' => '',
    ] + parent::defineDefaultProperties();
  }

  /* ************************************************************************ */

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    return $form;
  }

}

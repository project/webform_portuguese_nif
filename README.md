# Webform Portuguese NIF

Provides functionality for collecting, validating and displaying portuguese NIF numbers in a Webform.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/webform_portuguese_nif).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/webform_portuguese_nif).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. No configuration needed, just install and enable the module and the
   field will be available on your webforms under "Advanced Elements"


## Maintainers

Current maintainers:
- João Mauricio - [jmauricio](https://www.drupal.org/u/jmauricio)
